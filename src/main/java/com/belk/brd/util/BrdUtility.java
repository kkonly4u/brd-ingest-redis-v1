package com.belk.brd.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.belk.brd.model.Attributes;
import com.belk.brd.model.Brd;
import com.belk.brd.model.BrdModel;
import com.belk.brd.repo.BrdRedisRepo;

import io.lettuce.core.api.sync.RedisCommands;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class BrdUtility {
	
	@Autowired
	private RedisCommands<String, String> redisCommands;
	
	/*
	public String getQuery(List<String> brdnumbers) {
		String queryString = "";
		
		if (null != brdnumbers && brdnumbers.size() > 0) {
			StringBuilder sbf = new StringBuilder();
			//sbf.append("SELECT * FROM /brdsbx WHERE brd.brdnumber=");
			sbf.append("SELECT * FROM /brd_master WHERE brd.brdnumber=");
			sbf.append("'");
			sbf.append(brdnumbers.get(0));
			sbf.append("'");

			for (int i = 1; i < brdnumbers.size(); i++) {
				sbf.append(" OR brd.brdnumber='");
				sbf.append(brdnumbers.get(i));
				sbf.append("'");
			}

			queryString = sbf.toString();
		}
		
		log.info(" Final Query  " + queryString);
		return queryString;
	}
*/
	/*
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<BrdModel> getDataFromPCC(String queryString) {
		
		List<BrdModel> data = new ArrayList();
		if (!queryString.isEmpty()) {
			QueryService queryService = gemfireCache.getQueryService();
			Query query = queryService.newQuery(queryString);
			SelectResults results = null;
			try {
				results = (SelectResults) query.execute();
				for (Iterator iter = results.iterator(); iter.hasNext();) {
					BrdModel data1 = (BrdModel) iter.next();
					data.add(data1);
				}
				System.out.print("Query Success");
			} catch (FunctionDomainException e) {
				e.printStackTrace();
			} catch ( e) {
				e.printStackTrace();
			} catch (NameResolutionException e) {
				e.printStackTrace();
			} catch (QueryInvocationTargetException e) {
				e.printStackTrace();
			}
		}
		return data;
	}
*/
	
	public boolean isBrdExpired(String expDt, String QC) {
		//Version - 1
		/*
		boolean isBrdExpired = false;
		if (null != expDt && !expDt.isEmpty()) {
			SimpleDateFormat expireDtFormatter = new SimpleDateFormat("yyyyMMdd");
			String expirydate = expDt;
			try {
				Date expDate = expireDtFormatter.parse(expirydate);
				if (QC.equalsIgnoreCase("95")) {
					Calendar currentDateCal = Calendar.getInstance();
					currentDateCal.setTimeZone(TimeZone.getTimeZone("America/New_York"));
					int date = currentDateCal.get(Calendar.DATE);
					int year = currentDateCal.get(Calendar.YEAR);
					int month = currentDateCal.get(Calendar.MONTH) + 1;
					int monthLength = String.valueOf(month).length();
					String strMonth = month + "";
					if (monthLength == 1) {
						strMonth = "0" + month;
					}
					String strDate = date + "";
					int dateLength = String.valueOf(date).length();
					if (dateLength == 1) {
						strDate = "0" + date;
					}
					String currentDate = year + "" + strMonth + "" + strDate;
					if (Integer.parseInt(currentDate) > Integer.parseInt(expDt)) {
						isBrdExpired = true;
						log.info("95 Expired");
					}
				} else {
					// other than 95
					Date currentDate = new Date();
					long diffInMillies = (currentDate.getTime() - expDate.getTime());
					long daysDiff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
					log.info("daysDiff: "+daysDiff);
					if (daysDiff > 31) {
						isBrdExpired = true;
						log.info("other than 95 Expired");
					}
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return isBrdExpired;
		*/
		
		//Version - 2 
		/*
		boolean isBrdExpired = false;
		if (null != expDt && !expDt.isEmpty()) {
			ZoneId etZoneId = ZoneId.of("America/New_York");
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");

			// Expiry Date Time
			LocalDate localExpiryDate = LocalDate.parse(expDt, formatter);
			LocalDateTime localExpiryDateTime = LocalDateTime.of(localExpiryDate, LocalDateTime.now().toLocalTime());
			ZonedDateTime zonedExpiryESTDateTime = localExpiryDateTime.atZone(etZoneId);

			// Current Date Time
			LocalDateTime localCurrentDateTime = LocalDateTime.now();
			ZonedDateTime zonedCurrentESTDateime = localCurrentDateTime.atZone(etZoneId);

			//Days between Expiry Date and Current Date
			long noOfDaysBetween = zonedExpiryESTDateTime.until(zonedCurrentESTDateime, ChronoUnit.DAYS);
			log.info("noOfDaysBetween: "+ noOfDaysBetween);
			
			if (QC.equalsIgnoreCase("95")) {
				//long difference = zonedCurrentESTDateime.truncatedTo(ChronoUnit.DAYS).compareTo(zonedExpiryESTDateTime.truncatedTo(ChronoUnit.DAYS));
				if (noOfDaysBetween > 0) {
					isBrdExpired = true;
					log.info("95 Expired");
				}
			} else {
				// other than 95
				//long noOfDaysBetween = zonedExpiryESTDateTime.until(zonedCurrentESTDateime, ChronoUnit.DAYS);
				if (noOfDaysBetween > 31) {
					isBrdExpired = true;
					log.info("other than 95 Expired");
				}
			}
		}
		return isBrdExpired;
	*/
		
	//Version - 3
		boolean isBrdExpired = false;
		if (null != expDt && !expDt.isEmpty()) {
			ZoneId etZoneId = ZoneId.of("America/New_York");
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");

			// In pivotal - Local time is UTC
			LocalDate localExpiryDate = LocalDate.parse(expDt, formatter);
			ZonedDateTime estExpiryDateTime = localExpiryDate.atStartOfDay(etZoneId);

			// Current Date Time
			Instant utcCurrentTime = Instant.now();
			ZonedDateTime estCurrentDateTime = utcCurrentTime.atZone(etZoneId);

			log.info(
					"\n localExpiryDate =====: {} \n estExpiryDateTime =====: {} \n utcCurrentTime =====: {} \n estCurrentDateTime =====: {}",
					localExpiryDate, estExpiryDateTime, utcCurrentTime, estCurrentDateTime);

			// Days between Expiry Date and Current Date
			long noOfDaysBetween = estExpiryDateTime.until(estCurrentDateTime, ChronoUnit.DAYS);
			log.info("noOfDaysBetween: {}", noOfDaysBetween);

			if (QC.equalsIgnoreCase("95")) {
				if (noOfDaysBetween > 0) {
					isBrdExpired = true;
					log.info("95 Expired");
				}
			} else {
				// other than 95
				if (noOfDaysBetween > 30) {
					isBrdExpired = true;
					log.info("other than 95 Expired");
				}
			}
		}
		return isBrdExpired;	
	}

	public String getFinalDate(String expDt) {
		// 02\/28\/2019
		String finaldate = "0";
		if (null != expDt && !expDt.isEmpty()) {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
			SimpleDateFormat dateResonseFormatter = new SimpleDateFormat("MM\\/dd\\/yyyy");
			try {
				Date forma_date = formatter.parse(expDt);
				finaldate = dateResonseFormatter.format(forma_date);
				log.info("finaldate is " + finaldate);

			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return finaldate;
	}

	public Date getRedemption_Dt(String redemption_Dt) {

		Date forma_date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");

		if (null != redemption_Dt && !redemption_Dt.isEmpty()) {
			try {
				if (!"0".equals(redemption_Dt.trim())) {
					forma_date = formatter.parse(redemption_Dt);

				} else {
					forma_date = formatter.parse(formatter.format(forma_date));
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return forma_date;
	}

	public boolean save(BrdModel brdData, Brd brd, Attributes attributes, List<BrdModel> data,
			BrdRedisRepo brdRepository, int i) {
		brdData.setBrdnumber(data.get(i).getBrd().getBrdnumber()+"|"+data.get(i).getBrd().getTokennumber());
		brd.setBrdnumber(data.get(i).getBrd().getBrdnumber());
		brd.setTokennumber(data.get(i).getBrd().getTokennumber());
		brd.setAttributes(attributes);
		brdData.setBrd(brd);
		
		Map<String, String> map = brdToHashMap(brdData);

//		Jedis jedis = new Jedis("localhost", 6379);
		// Version-1 :: Using traditional for loop to iterate the map and using hset
		/*
		for (Map.Entry<String, String> entry : map.entrySet()) {
			System.out.format("%s=%s", entry.getKey(), entry.getValue());
			jedis.hset("brdnumber:" + brdData.getBrd().getBrdnumber(), entry.getKey(), entry.getValue());
		}
		*/
		
		// Version-2 Java8 :: using hset		
		// map.forEach((k, v) -> jedis.hset("brdnumber:" + brdData.getBrd().getBrdnumber(), k, v));
		
		// Version-3 :: Using hmset directly
//		jedis.hmset("brdnumber:" + brdData.getBrd().getBrdnumber(), map);
		
		redisCommands.hmset("brdnumber:" + brdData.getBrd().getBrdnumber(), map);
		
		// appending to stream
//		jedis.xadd("mystream-0", StreamEntryID.NEW_ENTRY, map);
		redisCommands.xadd("mystream-0",map);
		
		//brdRepository.save(brdData);
		return true;
	}

	public Map<String, String> brdToHashMap(BrdModel brdData) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("brdnumber", brdData.getBrd().getBrdnumber() != null ? brdData.getBrd().getBrdnumber() : "");
		map.put("tokennumber", brdData.getBrd().getTokennumber() != null ? brdData.getBrd().getTokennumber() : "");
		map.put("issue_date", brdData.getBrd().getAttributes().getIssueDate() != null ? brdData.getBrd().getAttributes().getIssueDate() : "");
		map.put("expiration_date", brdData.getBrd().getAttributes().getExpirationDate() != null ? brdData.getBrd().getAttributes().getExpirationDate() : "");
		map.put("redemption_dt", brdData.getBrd().getAttributes().getRedemptionDt() != null ?brdData.getBrd().getAttributes().getRedemptionDt() : "");
		map.put("cycle_amt_rewarded", brdData.getBrd().getAttributes().getCycleAmtRewarded() != null ? brdData.getBrd().getAttributes().getCycleAmtRewarded() : "");
		map.put("amt_reward_used", brdData.getBrd().getAttributes().getAmtRewardUsed() != null ? brdData.getBrd().getAttributes().getAmtRewardUsed() : "");
		map.put("purchase_amt", brdData.getBrd().getAttributes().getPurchaseAmt() != null ? brdData.getBrd().getAttributes().getPurchaseAmt() : "");
		map.put("purchase_store", brdData.getBrd().getAttributes().getPurchaseStore()!= null ? brdData.getBrd().getAttributes().getPurchaseStore() : "");
		map.put("register_number", brdData.getBrd().getAttributes().getRegisterNumber()!= null ? brdData.getBrd().getAttributes().getRegisterNumber() : "");
		map.put("trans_num", brdData.getBrd().getAttributes().getTransNum()!= null ? brdData.getBrd().getAttributes().getTransNum() : "");
		map.put("reason_code", brdData.getBrd().getAttributes().getReasonCode()!= null ? brdData.getBrd().getAttributes().getReasonCode() : "");
		map.put("redemption_account_number", brdData.getBrd().getAttributes().getRedemptionAccountNumber()!= null ? brdData.getBrd().getAttributes().getRedemptionAccountNumber() : "");
		map.put("last_maintained_userid", brdData.getBrd().getAttributes().getLastMaintainedUserid()!= null ? brdData.getBrd().getAttributes().getLastMaintainedUserid() : "");
		map.put("web_hold_flag", brdData.getBrd().getAttributes().getWebHoldFlag()!= null ? brdData.getBrd().getAttributes().getWebHoldFlag() : "");
		map.put("web_hold_date", brdData.getBrd().getAttributes().getWebHoldDate()!= null ? brdData.getBrd().getAttributes().getWebHoldDate() : "");
		map.put("batch_flag", brdData.getBrd().getAttributes().getBatchFlag()!= null ? brdData.getBrd().getAttributes().getBatchFlag() : "");
		map.put("action_flag", brdData.getBrd().getAttributes().getActionFlag()!= null ? brdData.getBrd().getAttributes().getActionFlag() : "");
		map.put("last_modified_date", brdData.getBrd().getAttributes().getLastModifiedDate()!= null ? brdData.getBrd().getAttributes().getLastModifiedDate() : "");
		map.put("type_cd", brdData.getBrd().getAttributes().getTypeCd()!= null ? brdData.getBrd().getAttributes().getTypeCd() : "");
		map.put("created_date", brdData.getBrd().getAttributes().getCreatedDate()!= null ? brdData.getBrd().getAttributes().getCreatedDate() : "");
		return map;
	}

	public String getRegisterNumber(String orderNum) {
		return getRegAndTranNum(orderNum)[0];
	}

	public String getTransNum(String orderNum) {
		return getRegAndTranNum(orderNum)[1];
	}

	public String[] getRegAndTranNum(String orderNum) {
		String[] strSubstrings = {};
		if (null != orderNum && !orderNum.isEmpty() && orderNum.length() == 8) {
			strSubstrings = orderNum.split("(?<=\\G.{4})");
			// log.info(Arrays.toString(strSubstrings));
		} else {
			strSubstrings = new String[2];
			strSubstrings[0] = "";
			strSubstrings[1] = "";
		}
		log.info("Register Num " + strSubstrings[0]);
		log.info("Tran Num " + strSubstrings[1]);
		return strSubstrings;
	}

	public boolean isRedemption_DtGtZero(String redemption_Dt) {
		// not zero is alwyas greater than zero
		if (null != redemption_Dt && !"0".equals(redemption_Dt.trim())) {
			return true;
		}
		return false;
	}

	public String setExpiredResponse(String expDt, String QC) {
		String brdResp = "000";
		if (isBrdExpired(expDt, QC)) {
			brdResp = "001";
		}
		return brdResp;
	}
	
	public void hashMapToBrd(Brd brd, Attributes attributes, Map.Entry<String, String> entry) {
		switch (entry.getKey()) {
			case "type_cd": {
				attributes.setTypeCd(entry.getValue());
				break;
			}
			case "web_hold_date": {
				attributes.setWebHoldDate(entry.getValue());
				break;
			}
			case "last_modified_date": {
				attributes.setLastModifiedDate(entry.getValue());
				break;
			}
			case "web_hold_flag": {
				attributes.setWebHoldFlag(entry.getValue());
				break;
			}
			case "action_flag": {
				attributes.setActionFlag(entry.getValue());
				break;
			}
			case "expiration_date": {
				attributes.setExpirationDate(entry.getValue());
				break;
			}
			case "batch_flag": {
				attributes.setBatchFlag(entry.getValue());
				break;
			}
			case "redemption_dt": {
				attributes.setRedemptionDt(entry.getValue());
				break;
			}
			case "reason_code": {
				attributes.setReasonCode(entry.getValue());
				break;
			}
			case "tokennumber": {
				brd.setTokennumber(entry.getValue());
				break;
			}
			case "purchase_amt": {
				attributes.setPurchaseAmt(entry.getValue());
				break;
			}
			case "issue_date": {
				attributes.setIssueDate(entry.getValue());
				break;
			}
			case "trans_num": {
				attributes.setTransNum(entry.getValue());
				break;
			}
			case "register_number": {
				attributes.setRegisterNumber(entry.getValue());
				break;
			}
			case "redemption_account_number": {
				attributes.setRedemptionAccountNumber(entry.getValue());
				break;
			}
			case "amt_reward_used": {
				attributes.setAmtRewardUsed(entry.getValue());
				break;
			}
			case "last_maintained_userid": {
				attributes.setLastMaintainedUserid(entry.getValue());
				break;
			}
			case "created_date": {
				attributes.setCreatedDate(entry.getValue());
				break;
			}
			case "cycle_amt_rewarded": {
				attributes.setCycleAmtRewarded(entry.getValue());
				break;
			}
			case "brdnumber": {
				brd.setBrdnumber(entry.getValue());
				break;
			}
			case "purchase_store": {
				attributes.setPurchaseStore(entry.getValue());
				break;
			}
		}
	}
}
