package com.belk.brd.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;

import io.lettuce.core.RedisClient;
import io.lettuce.core.RedisURI;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;

@Configuration
@EnableRedisRepositories
public class RedisConfiguration {

	@Bean
	public RedisCommands<String, String> getRedisCommands() {
//		RedisClient redisClient = RedisClient.create("redis://rdb:rdb@34.74.75.117:11630");
		RedisURI redisURI = RedisURI.Builder.redis("34.74.75.117", 11630)
				.withDatabase(0).withPassword("rdb").build();
		RedisClient redisClient = RedisClient.create(redisURI);
		StatefulRedisConnection<String, String> connection = redisClient.connect();
		return connection.sync();
	}
}
