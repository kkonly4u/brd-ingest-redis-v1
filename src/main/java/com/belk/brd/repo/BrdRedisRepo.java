package com.belk.brd.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.belk.brd.model.BrdModel;

@Repository
public interface BrdRedisRepo extends CrudRepository<BrdModel, String> {}
