package com.belk.brd.service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.belk.brd.model.BrdModel;
import com.belk.brd.util.BrdUtility;

import io.lettuce.core.api.sync.RedisCommands;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class BrdIngestService {

	@Autowired
	private RedisCommands<String, String> redisCommands;
	
	@Autowired
	private BrdUtility brdUtility;
	
	private final String BRDNUMBER_REDIS_KEY_PREFIX = "brdnumber:";
	private final String TOKENNUMBER_REDIS_KEY_PREFIX = "tokennumber:";

	private AtomicInteger score = new AtomicInteger();
	
	public String addAll(List<BrdModel> brdData) {
		brdData.stream().forEach(model -> {

			// Convert Object to Map
			Map<String, String> map = brdUtility.brdToHashMap(model);

			// adding data
			redisCommands.hmset(BRDNUMBER_REDIS_KEY_PREFIX + model.getBrd().getBrdnumber(), map);

			// secondary Index. Adding hashes references to set
			/*
			 * redisCommands.sadd(TOKENNUMBER_REDIS_KEY_PREFIX +
			 * model.getBrd().getTokennumber(), BRDNUMBER_REDIS_KEY_PREFIX +
			 * model.getBrd().getBrdnumber());
			 */

			// secondary Index. Adding hashes references to sorted set
			redisCommands.zadd(TOKENNUMBER_REDIS_KEY_PREFIX + model.getBrd().getTokennumber(), score.incrementAndGet(),
					BRDNUMBER_REDIS_KEY_PREFIX + model.getBrd().getBrdnumber());

			// posting the data to stream
			redisCommands.xadd("mystream-0", map);
		});
		log.debug("Saved all");
		return "** Saved All and posted to Redis Stream **";
	}

}
