package com.belk.brd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BrdIngestRedisApplication {

	public static void main(String[] args) {
		SpringApplication.run(BrdIngestRedisApplication.class, args);
	}

}
