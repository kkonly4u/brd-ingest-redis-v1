package com.belk.brd.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.belk.brd.model.BrdModel;
import com.belk.brd.service.BrdIngestService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class BrdIngestController {

	private final BrdIngestService service;

	@Autowired
	public BrdIngestController(BrdIngestService service) {
		this.service = service;
	}

	@PostMapping(path = "/brd/addAll")
	public String addAll(@RequestBody List<BrdModel> brdData) {
		log.info("Inside brd all method");
		return service.addAll(brdData);
	}
}
